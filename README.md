Add this code to the file `sites/all/modules/profile2/profile2.tpl.php`

```
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>>
        <a href="<?php print $url; ?>"><?php print $title; ?></a>
    </h2>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      if (arg(0) == 'user' && is_numeric(arg(1))) {
         $fields = visiblefield_check_data(arg(1));
         foreach ($fields as $field => $enable) {
            if ($enable == 0) {
              hide($content[$field]);
            }
         }
         print render($content);
      }
      else {
        print render($content);
      }
    ?>
  </div>
</div>
```
